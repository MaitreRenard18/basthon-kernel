#!/usr/bin/env python3
import urllib
from pathlib import Path

AUTHOR = 'Romain Casati'
SITENAME = 'Basthon'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

THEME = 'theme'

DIRECT_TEMPLATES = ('index', 'desktop', 'about', 'galerie', 'doc', 'contrib')

STATIC_PATHS = [
    '.htaccess',
]

EXTRA_PATH_METADATA = {
    '.htaccess': {'path': '.htaccess'},
}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = []

# Social widget
SOCIAL = []

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True


def readme(*args):
    import markdown
    with open("../README.md") as f:
        return markdown.markdown(f.read(), extensions=['attr_list', 'tables'])


def desktop(*args):
    from urllib.request import urlopen
    import markdown
    with urlopen("https://framagit.org/basthon/basthon-desktop/-/raw/master/README.md") as f:
        md = f.read().decode('utf-8')
    return markdown.markdown(md, extensions=['tables'])


def galerie(*args):
    from galerie import examples
    SITEURL = args[0]
    one_img = """
<div class="gallery-item">
  <div style="position: relative; top: 0; left: 0;">
    <a target="_blank" href="{SITEURL}/theme/assets/img/galerie/{language}/{img_name}" style="position: relative; top: 0; left: 0; cursor: zoom-in;">
      <img src="{SITEURL}/theme/assets/img/galerie/.thumbnails/{language}/{img_name}" title="Voir l'image">
    </a>
    <a target="_blank" class="shadowed" href="{url}" style="position: absolute; top: calc(50% - 25px); left: calc(50% - 25px);">
      <img src="{SITEURL}/theme/assets/img/play.svg" style="width: 50px;" title="Voir le code dans Basthon" class="python-yellow"/>
    </a>
  </div>
  <div class="desc">{description}</div>
</div>"""

    def url(file, language, aux, module, extensions):
        url = str(Path("examples") / file)
        domain = 'notebook' if file.endswith('.ipynb') else 'console'
        language = f"kernel={language}&" if language != "python3" else ""
        extensions = f"extensions={','.join(extensions)}&" if extensions else ""
        url = f"https://{domain}.basthon.fr/?{language}{extensions}from={urllib.parse.quote(url)}"
        if aux is not None:
            if isinstance(aux, str):
                aux = [aux]
            for a in aux:
                url += f"&aux={urllib.parse.quote(a)}"
        if module is not None:
            if isinstance(module, str):
                module = [module]
            for m in module:
                url += f"&module={urllib.parse.quote(m)}"
        return url

    def one_section(language, html):
        return f'<section class="tab-panel"><div class="gallery">{html}</div></section>'

    return "".join(one_section(language, "".join(
        one_img.format(SITEURL=SITEURL,
                       img_name=f"{Path(file).with_suffix('')}.png",
                       description=data['desc'],
                       url=url(file, language.lower().replace(' ', ''),
                               data.get('aux'),
                               data.get('module'),
                               data.get('extensions')),
                       language=language.lower().replace(' ', ''))
        for file, data in files.items()))
                   for language, files in examples.items())


JINJA_FILTERS = {"readme": readme,
                 "desktop": desktop,
                 "galerie": galerie}
