(auto-evaluation)=

# Auto-évaluation

Le principe de l'auto-évaluation dans Basthon (et son module {mod}`basthon.autoeval`)
a principalement été développé pour une utilisation dans Capytale.

Nous invitons le lecteur à se référer à la
[documentation de Capytale à ce sujet](https://capytale.forge.apps.education.fr/basthon/basthon-capytale/parts/auto-evaluation.html).
