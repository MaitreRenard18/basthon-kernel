from pathlib import Path
import base64
import time
from urllib.parse import quote
import re
from utils import same_content


def test_all(selenium_py3):
    # ensure all basthon modules are tested
    tested = set(
        g[len("test_") :]
        for g in globals()
        if g.startswith("test_") and g != "test_all"
    )
    data = selenium_py3.run_basthon(
        """
    from basthon import kernel
    kernel.list_basthon_modules(True)"""
    )
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    internal = set(eval(result["result"]["text/plain"]))
    assert tested == internal


def test_turtle(selenium_py3):
    selenium_py3.run_basthon(
        """
    import turtle
    turtle.forward(100)
    turtle.done()
    """,
        return_data=False,
    )
    # can't access content like this
    # elem = result['content']
    # because of selenium's StaleElementReferenceException
    # bypassing it via JS
    svg = selenium_py3.run_js(
        "return window._basthon_eval_data.display.content.outerHTML"
    )
    svg = re.sub('"af_[0-9a-f]+_', '"af_', svg)
    assert same_content(selenium_py3, "python3_turtle.svg", svg)

    # with animation disabled
    selenium_py3.run_basthon(
        """
    turtle.animation("off")
    turtle.forward(100)
    turtle.done()
    """,
        return_data=False,
    )
    svg = selenium_py3.run_js(
        "return window._basthon_eval_data.display.content.outerHTML"
    )
    svg = re.sub('"af_[0-9a-f]+_', '"af_', svg)
    assert same_content(selenium_py3, "python3_turtle-anim-off.svg", svg)


def test_graphviz(selenium_py3):
    result = selenium_py3.run_basthon(
        """
    from graphviz import Digraph
    import basthon

    g = Digraph('G')
    g.edge('Hello', 'World')
    basthon.display(g)
    print(g.source)
    """
    )
    assert result["stderr"] == ""
    assert result["stdout"] == "digraph G {\n\tHello -> World\n}\n"
    assert result["display"]["display_type"] == "multiple"
    svg = result["display"]["content"]["image/svg+xml"]
    assert same_content(selenium_py3, "python3_graphviz.svg", svg)


def test_requests(selenium_py3):
    result = selenium_py3.run_basthon(
        """
    import requests

    url = "http:///nghttp2.org/httpbin/html"
    response = requests.get(url)
    print(response.text)
    response.headers
    """
    )
    assert result["stderr"] == ""
    request = result["stdout"]
    assert same_content(selenium_py3, "python3_requests.txt", request)
    headers = eval(result["result"]["result"]["text/plain"])
    assert headers["content-type"] == "text/html; charset=utf-8"


def test_proj4py(selenium_py3):
    result = selenium_py3.run_basthon(
        """
    import proj4py

    # from WGS84 to Lambert93
    proj = proj4py.proj4('EPSG:4326', 'EPSG:2154')
    proj.forward((46.57824382381372, 2.468613626422624))
    """
    )
    assert result["stderr"] == ""
    assert result["stdout"] == ""
    target = eval(result["result"]["result"]["text/plain"])
    assert target == (659306.8946611215, 6608826.400123728)


def test_IPython(selenium_py3):
    result = selenium_py3.run_basthon(
        """
    import IPython
    import IPython.display
    from IPython.display import display, display_image, IFrame, Markdown
    Markdown("$\\sqrt{2}$")
    """
    )
    assert result["stderr"] == ""
    assert result["stdout"] == ""
    result = result["result"]
    result["result"]["text/markdown"] == "$\\sqrt{2}$"


def test_p5(selenium_py3):
    selenium_py3.run_basthon(
        """
    from p5 import *

    x = 100
    y = 100

    def setup():
        createCanvas(200, 200)

    def draw():
        background(0)
        fill(255)
        rect(x, y, 50, 50)

    run()
    """,
        return_data=False,
    )
    basename = "python3_p5"
    # can't access content like this
    # elem = result['content']
    # because of selenium's StaleElementReferenceException
    # bypassing it via JS
    html = selenium_py3.run_js(
        "return window._basthon_eval_data.display.content.outerHTML"
    )
    assert same_content(selenium_py3, f"{basename}.html", html)
    selenium_py3.run_js(
        "document.body.appendChild(window._basthon_eval_data.display.content);"
    )
    # this should be replaced with a clean selenium wait
    time.sleep(1)
    png = selenium_py3.run_js(
        """
    const elem = window._basthon_eval_data.display.content;
    return elem.getElementsByTagName('canvas')[0].toDataURL('image/png');
    """
    )
    png = base64.b64decode(png[len("data:image/png;base64,") :])
    assert same_content(selenium_py3, f"{basename}.png", png, binary=True)

    selenium_py3.run_basthon("stop()")


def test_tutor(selenium_py3):
    result = selenium_py3.run_basthon(
        """
from tutor import tutor  # with a comment here it should work

a = 5
a = a + 1

tutor()"""
    )
    assert result["stderr"] == ""
    assert result["stdout"] == ""
    assert result["display"]["display_type"] == "tutor"
    assert result["display"]["iframe-id"] == "basthon-pythontutor-iframe-0"
    # webserver starts at arbitrary port so this makes absolute paths
    # inconsistent between calls...
    # we remove the host:port part from URLs
    iframe = result["display"]["content"]
    basthon_root = selenium_py3.run_js("return Basthon.basthonRoot(true);")
    iframe = iframe.replace(quote(basthon_root), "")
    assert same_content(selenium_py3, "python3_tutor-iframe.html", iframe)


def test_rcviz(selenium_py3):
    from ast import literal_eval

    result = selenium_py3.run_basthon(
        """
from rcviz import viz
@viz
def fibo(n):
    if n < 2:
        return n
    return fibo(n - 1) + fibo(n - 2)

print(fibo(4))
fibo.callgraph()
print(fibo(5))
fibo.callgraph().source
"""
    )
    assert result["stderr"] == ""
    assert result["stdout"] == "3\n5\n"
    gv = literal_eval(result["result"]["result"]["text/plain"])
    gv = re.sub(r"[0-9]+ -> [0-9]+ \[label=", " ->  [label=", gv)
    gv = re.sub(r"[0-9]+ \[label=", " [label=", gv)
    assert same_content(selenium_py3, "python3_rcviz.gv", gv)
