from pathlib import Path
import re
import base64
from utils import same_content


def test_all(selenium_py3):
    # ensure all patched modules are tested
    tested = set(
        g[len("test_") :]
        for g in globals()
        if g.startswith("test_") and g != "test_all"
    )
    data = selenium_py3.run_basthon(
        """
    from basthon import _patch_modules
    set(g[len('patch_'):] for g in dir(_patch_modules)
        if g.startswith('patch_'))"""
    )
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    patched = eval(result["result"]["text/plain"])
    assert tested == patched


def test_pkg_resources(selenium_py3):
    # importing pkg_resources (setuptools) before using help to avoid warning
    # 'Distutils was imported before Setuptools'
    # FIXME: find a way to import setuptools before using help
    #       (async plays against us here)
    # see also test_patch_modules
    result = selenium_py3.run_basthon("import pkg_resources ; help('modules')")
    assert "result" not in result["result"]
    assert result["stderr"] == ""


def test_pydoc(selenium_py3):
    # importing pkg_resources (setuptools) before using help to avoid warning
    # 'Distutils was imported before Setuptools'
    # FIXME: find a way to import setuptools before using help
    #       (async plays against us here)
    # see also test_pkg_resources
    data = selenium_py3.run_basthon("import pkg_resources ; help('modules')")
    assert "result" not in data["result"]
    assert data["stderr"] == ""
    text = data["stdout"]
    assert same_content(selenium_py3, "python3_modules.txt", text)

    data = selenium_py3.run_basthon("help(str)")
    assert "result" not in data["result"] and data["stderr"] == ""
    assert len(data["stdout"]) == 15604


def test_time(selenium_py3):
    import time

    # async sleep
    t0 = time.perf_counter()
    result = selenium_py3.run_basthon("import time ; time.sleep(3)")
    t1 = time.perf_counter()
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert abs(t1 - t0 - 3) < 0.2

    # active sleep
    t0 = time.perf_counter()
    result = selenium_py3.run_basthon(
        """
def f(x):
    time.sleep(3)

f(3)
"""
    )
    t1 = time.perf_counter()
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert abs(t1 - t0 - 3) < 0.2


def test_PIL(selenium_py3):
    result = selenium_py3.run_basthon(
        """
    from PIL import Image, ImageDraw
    w, h = 120, 90
    bbox = [(10, 10), (w - 10, h - 10)]

    img = Image.new("RGB", (w, h), "#f9f9f9")
    dctx = ImageDraw.Draw(img)
    dctx.rectangle(bbox, fill="#ddddff", outline="blue")
    img.show()
    img.save("pil.png")
    """
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["display"]["display_type"] == "multiple"
    img = result["display"]["content"]["image/png"]
    assert same_content(selenium_py3, "python3_pil.png", img)


def test_matplotlib(selenium_py3):
    selenium_py3.run_basthon(
        """
    import matplotlib.pyplot as plt

    plt.figure()
    plt.plot([0, 1], [0, 1])
    plt.show()
    """,
        return_data=False,
    )
    # can't access content like this
    # elem = result['content']
    # because of selenium's StaleElementReferenceException
    # bypassing it via JS
    html = selenium_py3.run_js(
        "return window._basthon_eval_data.display.content.outerHTML;"
    )
    html = re.sub("matplotlib_[0-9a-f]+", "matplotlib_", html)
    assert same_content(selenium_py3, "python3_matplotlib.html", html)

    png = selenium_py3.run_js(
        "return window._basthon_eval_data.display.content.getElementsByTagName('canvas')[0].toDataURL('image/png');"
    )
    png = base64.b64decode(png[len("data:image/png;base64,") :])
    assert same_content(selenium_py3, "python3_matplotlib.png", png, binary=True)

    # case where plot and show are called separately
    selenium_py3.run_basthon(
        """
    plt.figure()
    plt.plot([0, 1], [0, 1])
     """,
        return_data=False,
    )
    selenium_py3.run_basthon("plt.show()", return_data=False)

    png2 = selenium_py3.run_js(
        "return window._basthon_eval_data.display.content.getElementsByTagName('canvas')[0].toDataURL('image/png');"
    )
    png2 = base64.b64decode(png2[len("data:image/png;base64,") :])
    assert png2 == png

    # testing animation
    data = selenium_py3.run_basthon(
        """
import matplotlib.pyplot as plt
import matplotlib.animation

fig = plt.figure()
ax = plt.axes(xlim=[-2, 2], ylim=[0, 4])
curve, = ax.plot([],[], '-b')

def animate(i):
    N = 2 * i + 3
    xx = [4 * k / (N - 1) - 2 for k in range(N)]
    yy = [x ** 2 for x in xx]
    curve.set_data(xx, yy)

ani = matplotlib.animation.FuncAnimation(fig, animate, frames=10, blit=False, interval=200)
ani"""
    )
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    html = result["result"]["text/html"]
    html = re.sub("_anim_img[0-9a-f]+", "_anim_img", html)
    html = re.sub("_anim_slider[0-9a-f]+", "_anim_slider", html)
    html = re.sub("_anim_radio[1-3]_[0-9a-f]+", "_anim_radio_", html)
    html = re.sub("_anim_loop_select[0-9a-f]+", "_anim_loop_select", html)
    html = re.sub("anim[0-9a-f]+", "anim", html)
    assert same_content(selenium_py3, "python3_matplotlib_animation.html", html)


def test_folium(selenium_py3):
    result = selenium_py3.run_basthon(
        """
    import folium

    m = folium.Map(location=[47.228382, 2.062796],
                   zoom_start=17)

    m.display()
    """
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["display"]["display_type"] == "multiple"
    map = re.sub("map_[0-9a-f]+", "map_", result["display"]["content"]["text/html"])
    map = re.sub("tile_layer_[0-9a-f]+", "tile_layer_", map)
    assert same_content(selenium_py3, "python3_folium.html", map)


def test_pandas(selenium_py3):
    result = selenium_py3.run_basthon(
        """
    import pandas as pd

    df = pd.DataFrame({
        'language': ["Python", "C", "Java"],
        'verbosity': [0.1, 0.5, 0.9]
    })

    df.display()
    """
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["display"]["display_type"] == "multiple"
    html = result["display"]["content"]["text/html"]
    assert same_content(selenium_py3, "python3_pandas.html", html)


def test_sympy(selenium_py3):
    result = selenium_py3.run_basthon(
        """
    import sympy
    from sympy.abc import x
    sympy.pretty_print(sympy.sqrt(x ** 2 + sympy.pi))
    """
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["display"]["display_type"] == "sympy"
    assert result["display"]["content"] == "$$\\sqrt{x^{2} + \\pi}$$"


def test_turtle(selenium_py3):
    # already tested in test_python3_basthon_modules
    assert True


def test_qrcode(selenium_py3):
    result = selenium_py3.run_basthon(
        """
    import qrcode

    code = qrcode.make("https://basthon.fr/", format='svg')
    code.show()
    """
    )
    assert result["stdout"] == ""
    assert result["stderr"] == ""
    assert result["display"]["display_type"] == "multiple"
    svg = result["display"]["content"]["image/svg+xml"]
    assert same_content(selenium_py3, "python3_qrcode-basthon.svg", svg)


def test_pyroutelib3(selenium_py3):
    result = selenium_py3.run_basthon(
        """
from pyroutelib3 import Router

depart = [47.08428480854615, 2.3939454203269217]
arrivee = [47.08187952227819, 2.398992969262373]

router = Router("foot")
node_depart = router.findNode(*depart)
node_arrivee = router.findNode(*arrivee)
status, route = router.doRoute(node_depart, node_arrivee)
status
"""
    )
    assert result["stdout"] == ""
    # TODO: remove this warning!
    assert (
        result["stderr"]
        == '/lib/python3.11/site-packages/filelock/__init__.py:32: UserWarning: only soft file lock is available\n  warnings.warn("only soft file lock is available")\n'
    )
    assert result["result"]["result"]["text/plain"] == "'success'"


def test_ipythonblocks(selenium_py3):
    result = selenium_py3.run_basthon(
        """
from ipythonblocks import BlockGrid

w = h = 10

grid = BlockGrid(w, h, block_size=4)

for block in grid:
    block.red = 255 * float(w - block.col) / w
    block.green = 255 * float(h - block.row) / h
    block.blue = 255 * block.col / w

grid.to_text()
grid.show_image()
"""
    )
    assert "result" not in result["result"]
    assert result["stderr"] == ""
    text = result["stdout"]
    assert same_content(selenium_py3, "python3_ipythonblocks_to_text.txt", text)
    assert result["display"]["display_type"] == "multiple"
    png = base64.b64decode(result["display"]["content"]["image/png"])
    assert same_content(
        selenium_py3, "python3_ipythonblocks_save_image.png", png, binary=True
    )


def test_cv2(selenium_py3):
    # opencv hangs CPU so we remove test
    pass
    # result = selenium_py3.run_basthon("""


# import cv2
# img = cv2.imread("pil.png")
# cv2.imshow("image", img)
#     """)
#     assert result['stdout'] == ""
#     assert result['stderr'] == ""
#     assert result['display']['display_type'] == 'multiple'
#     img = result['display']['content']['image/png']
#     assert same_content(selenium_py3, "python3_cv2.png", img)


def test_drawSvg(selenium_py3):
    data = selenium_py3.run_basthon(
        """
import drawSvg as draw

d = draw.Drawing(200, 100, origin='center', displayInline=False)

r = draw.Rectangle(-80,0,40,50, fill='#1248ff')
r.appendTitle('Our first rectangle')
d.append(r)

d
    """
    )
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    html = result["result"]["text/html"]
    assert same_content(selenium_py3, "python3_drawSvg.html", html)


def test_plotly(selenium_py3):
    reg = re.compile('<div id="([0-9a-f-]+)"')
    data = selenium_py3.run_basthon(
        """
import plotly.graph_objects as go

fig = go.Figure(
    data=[go.Bar(y=[2, 1, 3])],
    layout_title_text="A nice title"
)

fig"""
    )
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["result"]
    html = result["result"]["text/html"]
    id = reg.search(html).group(1)
    html = html.replace(id, "")
    assert same_content(selenium_py3, "python3_plotly_go.html", html)

    data = selenium_py3.run_basthon(
        """
import pandas
import plotly.express as px

fig = px.bar(x=["a", "b", "c"], y=[1, 3, 2])
fig.show()
"""
    )
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    result = data["display"]
    html = result["content"]["text/html"]
    id = reg.search(html).group(1)
    html = html.replace(id, "")
    assert same_content(selenium_py3, "python3_plotly_express.html", html)


def test_mocodo(selenium_py3):
    data = selenium_py3.run_basthon(
        """
from mocodo.magic import mocodo
"""
    )
    assert data["stdout"] == ""
    assert data["stderr"] == ""
    assert "result" not in data["result"]
