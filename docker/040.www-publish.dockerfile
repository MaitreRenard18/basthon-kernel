FROM debian:bookworm-slim

ENV DOCKER_IMAGE="www-publish"

RUN apt-get update
RUN apt-get install -y --no-install-recommends sudo npm make python3 python3-distutils python3-setuptools python3-pip \
  pelican imagemagick python3-pygments openssh-client rsync \
  latex-make texlive texlive-lang-french texlive-fonts-extra texlive-latex-extra texlive-science
RUN rm -rf /var/lib/apt/lists/*

COPY sphinx-requirements.txt .
RUN pip3 --no-cache-dir install --break-system-packages -r sphinx-requirements.txt && rm sphinx-requirements.txt

RUN echo 'PS1="\[\033[1;36m\](🐳 ${DOCKER_IMAGE})\[\033[0m\][\u@\h \W]\$ "' >> /etc/bash.bashrc
