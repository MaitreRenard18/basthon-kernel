#!/usr/bin/env bash

set -eo pipefail


DOCKER_IMAGE="casatir/basthon-kernel:$1"
DOCKER_COMMAND="/bin/bash"

USER_NAME="$(id -u -n)"
USER_HOME="/home/$USER_NAME"
USER_PASS="x"
USER_ID="$(id -u)"
USER_GID="$(id -g)"
USER_COMMENT_FIELD="${USER_NAME} user alias"
USER_INTERPRETER="/sbin/nologin"
USER_ACCOUNT_INFO="${USER_NAME}:${USER_PASS}:${USER_ID}:${USER_GID}:${USER_COMMENT_FIELD}:${USER_HOME}:${USER_INTERPRETER}"
GROUP_ACCOUNT_INFO="${USER_NAME}:x:${USER_GID}:"
PASS_HASH="${USER_NAME}:\$1\$ADUODeAy\$eCJ1lPSxhSGmSvrmWxjLC1:10000:0:99999:7:::"
WORKDIR="$USER_HOME/src"

# Start a detached container as root, add the host uname and uid to /etc/passwd,
# then run forever
CONTAINER=$(\
  docker run \
    --workdir "$WORKDIR" \
    -d --rm \
    -v "$PWD:$WORKDIR" \
    -v ${USER_HOME}/.Xauthority:${USER_HOME}/.Xauthority \
    -v /tmp/.X11-unix:/tmp/.X11-unix \
    -e DISPLAY \
    --net host \
    --user root \
    --shm-size 2g \
    "${DOCKER_IMAGE}" \
    /bin/bash -c " \
      echo '${USER_ACCOUNT_INFO}' >> /etc/passwd ; \
      echo '${GROUP_ACCOUNT_INFO}' >> /etc/group ; \
      echo '${PASS_HASH}' >> /etc/shadow ; \
      chown -R ${USER_NAME}: '${USER_HOME}' ; \
      adduser ${USER_NAME} sudo ; \
      tail -f /dev/null \
    " \
)

EXIT_STATUS=0
# execute the provided command as the host user with HOME=/home/user/
docker exec \
  -it \
  --user "$(id --user):$(id --group)" \
  "$CONTAINER" \
  /bin/bash -c "${DOCKER_COMMAND}" || EXIT_STATUS=$?

docker kill "$CONTAINER" > /dev/null
exit $EXIT_STATUS
