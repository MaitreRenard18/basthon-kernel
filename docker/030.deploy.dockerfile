FROM debian:bookworm-slim

ENV DOCKER_IMAGE="deploy"

RUN apt-get update
RUN apt-get install -y --no-install-recommends sudo nodejs npm jq git
RUN rm -rf /var/lib/apt/lists/*

RUN echo 'PS1="\[\033[1;36m\](🐳 ${DOCKER_IMAGE})\[\033[0m\][\u@\h \W]\$ "' >> /etc/bash.bashrc
