FROM debian:bookworm-slim

ENV DOCKER_IMAGE="build"

RUN apt-get update
RUN apt-get install -y --no-install-recommends sudo make git patch llvm nodejs npm xz-utils bzip2 python3 python3-distutils python3-setuptools python3-pip dune js-of-ocaml libjs-of-ocaml libjs-of-ocaml-dev libbase-ocaml-dev
RUN rm -rf /var/lib/apt/lists/*

RUN echo 'PS1="\[\033[1;36m\](🐳 ${DOCKER_IMAGE})\[\033[0m\][\u@\h \W]\$ "' >> /etc/bash.bashrc
