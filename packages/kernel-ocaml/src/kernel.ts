import { KernelBase } from "@basthon/kernel-base";
import { Toplevel } from "./toplevel_types";

declare global {
  interface Window {
    jsoo_runtime?: any;
  }
}

/**
 * An OCaml kernel that satisfies Basthon's API.
 */
export class KernelOCaml extends KernelBase {
  private __kernel__?: Toplevel;
  private __eval_data__?: any = {};
  private _initInnerCode: string;

  constructor(options: any) {
    super(options);
    this._initInnerCode = `\
open Js_of_ocaml
module Basthon = struct
  let download (path: string): unit = ignore((Js.Unsafe.eval_string "window.Basthon.__kernel__")##download path)
  let create_canvas () = (Js.Unsafe.eval_string "window.Basthon.__kernel__")##createcanvas()
  let display_canvas canvas: unit = ignore((Js.Unsafe.eval_string "window.Basthon.__kernel__")##displaycanvas canvas)
  let save_canvas canvas (path: string): unit = ignore((Js.Unsafe.eval_string "window.Basthon.__kernel__")##savecanvas canvas path)
  let download_canvas ?(format = "png") canvas: unit = ignore((Js.Unsafe.eval_string "window.Basthon.__kernel__")##downloadcanvas canvas format)
  let display_image (path: string): unit = ignore((Js.Unsafe.eval_string "window.Basthon.__kernel__")##displayimage path)
  let version () : string = (Js.Unsafe.eval_string "window.Basthon.__kernel__")##version()
  let help () : unit = print_endline {ext|\
Basthon module
  help:               Show this help.
  download path:      Download a file from the local filesystem.
  display_image path: Display a PNG image from the local filesystem.
  create_canvas:      Create a HTML5 canvas to be displayed with display_canvas.
  display_canvas canvas:   Display a HTML5 canvas created with create_canvas.
  save_canvas canvas path: Save a canvas to a PNG/JPG file to the local filesystem.
  download_canvas ?(format = "png") canvas: Download a canvas to a PNG/JPG file.
|ext}
end`;
  }

  public language() {
    return "ocaml";
  }
  public languageName() {
    return "OCaml";
  }
  public moduleExts() {
    return ["ml"];
  }

  /**
   * Start the Basthon kernel asynchronously.
   */
  public async launch() {
    const { __kernel__ } = await import("./__kernel__");
    this.__kernel__ = __kernel__;
    this.restart();
  }

  /**
   * Basthon async code evaluation function.
   */
  public async evalAsync(
    code: string,
    outCallback: (_: string) => void,
    errCallback: (_: string) => void,
    data: any = null
  ): Promise<any> {
    // force interactivity in all modes
    if (this.__kernel__ == null) return;
    if (this._execution_count === 0) this.__kernel__?.exec(this._initInnerCode);

    data.interactive = true;
    this.__eval_data__ = data;

    // set stream callbacks
    this.__kernel__.io.stdout = outCallback;
    this.__kernel__.io.stderr = errCallback;

    this._execution_count++;
    let result: string | object | undefined = this.__kernel__?.exec(code);

    // restoration
    this.__kernel__.io.stdout = console.log;
    this.__kernel__.io.stderr = console.error;

    // return result
    if (typeof result === "string" && result.length > 0)
      result = { "text/plain": result.replace(/\n$/, "") };
    else result = undefined;
    return [result, this._execution_count];
  }

  /**
   * Restart the kernel.
   */
  public restart() {
    if (this.__kernel__?.init() !== 0)
      throw new Error("Can't start OCaml kernel!");
    this._execution_count = 0;
  }

  /**
   * OCaml wrapper arround Kernel.download (to be called by __kernel__.ml).
   */
  public ocamlDownload(content: any, filename: string) {
    const array = window.jsoo_runtime.caml_convert_bytes_to_array(content);
    this.download(array, filename);
  }

  /**
   * Send eval.display event with the given canvas
   * and ocaml-canvas as display_type.
   */
  public displayCanvas(canvas: HTMLCanvasElement) {
    // simple copy to update evaluation data with display_data
    const display_data: any = JSON.parse(JSON.stringify(this.__eval_data__));
    display_data["display_type"] = "ocaml-canvas";
    display_data["content"] = canvas;
    this.dispatchEvent("eval.display", display_data);
  }

  /**
   * Save a canvas to a file on the local FS.
   */
  public async saveCanvas(canvas: HTMLCanvasElement, path: string) {
    if (this.__kernel__ == null) return;
    const ext = path.split(".").pop()?.toLowerCase();
    let mime = "image/png";
    if (ext === "jpg" || ext === "jpeg") mime = "image/jpeg";
    const blob: Blob | null = await new Promise((resolve) =>
      canvas.toBlob(resolve, mime)
    );
    if (blob == null) return;
    // await blob.arrayBuffer() seems not support for old browser version
    const content = await new Response(blob).arrayBuffer();
    this.__kernel__.createfile(path, content);
  }

  /**
   * Download a canvas as an image file (png or jpg).
   */
  public downloadCanvas(canvas: HTMLCanvasElement, format: string) {
    if (
      format == null ||
      !(typeof format === "string" || (format as any) instanceof String)
    )
      format = "png";
    format = format.toLowerCase();
    let mime: string;
    switch (format) {
      case "jpg":
      case "jpeg":
        mime = "image/jpeg";
        break;
      case "png":
      default:
        mime = "image/png";
        // we force png
        format = "png";
        break;
    }
    const image = canvas.toDataURL(mime).replace(mime, "image/octet-stream");
    this.download(image, `canvas.${format}`);
  }

  /**
   * Display a PNG image.
   */
  public async displayImage(content: any) {
    // simple copy to update evaluation data with display_data
    const display_data: any = JSON.parse(JSON.stringify(this.__eval_data__));
    const array = window.jsoo_runtime.caml_convert_bytes_to_array(content);
    // convert array to
    const blob = new Blob([array], { type: "image/png" });
    const dataURL: string = await new Promise((resolve) => {
      const reader = new FileReader();
      reader.onloadend = () => resolve(reader.result as string);
      reader.readAsDataURL(blob);
    });
    const png = dataURL.slice("data:image/png;base64,".length);
    display_data["display_type"] = "multiple";
    display_data["content"] = { "image/png": png };
    this.dispatchEvent("eval.display", display_data);
  }

  /**
   * Put a file on the local (emulated) filesystem.
   */
  public async putFile(filename: string, content: ArrayBuffer) {
    if (this.__kernel__ == null) return;
    this.__kernel__.createfile(filename, content);
  }

  /**
   * Put an importable module on the local (emulated) filesystem
   * and load dependencies.
   */
  public async putModule(filename: string, content: ArrayBuffer) {
    this.putFile(filename, content);
    /* why is this needed?
     * even if path is already added using the #directory directive,
     * one should recall the directive each time the folder is modified...
     */
    this.__kernel__?.loadmodule(filename);
  }

  /**
   * List modules launched via putModule.
   */
  public userModules() {
    return [];
  }

  /**
   * Download a file from the VFS.
   */
  public getFile(path: string): Uint8Array {
    return new Uint8Array([]);
  }

  /**
   * Download a user module file.
   */
  public getUserModuleFile(filename: string): Uint8Array {
    return new Uint8Array([]);
  }

  /**
   * Is the source ready to be evaluated or want we more?
   * Usefull to set ps1/ps2 in teminal prompt.
   */
  public more(source: string) {
    return false;
  }

  /**
   * Mimic the OCaml's REPL banner.
   */
  public banner() {
    return "        OCaml version 4.11.1\n";
  }

  public ps1(): string {
    return "# ";
  }

  public ps2(): string {
    return "  ";
  }

  /**
   * Complete a code at the end (usefull for tab completion).
   *
   * Returns an array of two elements: the list of completions
   * and the start index.
   */
  public complete(code: string): [string[], number] | [] {
    return [];
  }
}
