import { KernelBase } from "@basthon/kernel-base";
import initSqlJs from "sql.js";

// see https://github.com/sql-js/react-sqljs-demo/blob/master/src/App.js
//@ts-ignore
import sqlWasm from "sql.js/dist/sql-wasm.wasm?asset-url";

export class KernelSQL extends KernelBase {
  private _dbInit: any;
  private _sqlsInit: string[];
  private SQL: any;
  private db: any;
  private _dots: { [key: string]: { [key: string]: string | (() => unknown) } };

  public constructor(options: any) {
    super(options);
    this._execution_count = 0;
    // initial db file (used to restart SQLite)
    this._dbInit = undefined;
    // initial SQL strings (used to restart SQLite)
    this._sqlsInit = [];
    // dot commands
    this._dots = {
      ".backup": {
        callback: this._export.bind(this),
        help: "Download as binary file",
      },
      ".dump": {
        callback: this._dump.bind(this),
        help: "Download as SQL file",
      },
      ".export": {
        callback: this._export.bind(this),
        help: "See .backup",
      },
      ".help": {
        callback: this._help.bind(this),
        help: "Show this help",
      },
      ".save": {
        callback: this._export.bind(this),
        help: "See .backup",
      },
      ".schema": {
        callback: this._schema.bind(this),
        help: "Show the CREATE statements",
      },
      ".tables": {
        callback: this._tables.bind(this),
        help: "List names of tables",
      },
    };
  }

  language() {
    return "sql";
  }
  languageName() {
    return "SQL";
  }
  moduleExts() {
    return ["sql", "db", "sqlite"];
  }

  public async launch() {
    this.SQL = await initSqlJs({ locateFile: () => sqlWasm });
    this.restart();
  }

  /**
   * Troncate json table to 150 items.
   */
  private static troncate_table(json: any) {
    const max_size = 150;
    if (json.values.length >= max_size) {
      const half_length = Math.floor(max_size / 3);
      const head = json.values.slice(0, half_length);
      const tail = json.values.slice(-half_length);
      json.values = head.concat([json.columns.map(() => "⋮ ")], tail);
    }
  }

  /**
   * HTML version of a table (useful in notebook).
   */
  private static html_table(json: any) {
    this.troncate_table(json);
    const columns = json.columns.map((c: string) => `<th>${c}</th>`).join("");
    const head = `<thead><tr style="text-align: right;">${columns}</tr></thead>`;
    const values = json.values
      .map(
        (r: string[]) =>
          "<tr>" + r.map((v) => `<td>${v}</td>`).join("") + "</tr>"
      )
      .join("");
    const body = `<tbody>${values}</tbody>`;
    return `<table class="dataframe" border="1">${head}${body}</table>`;
  }

  /**
   * Text version of a table.
   */
  private static text_table(json: any) {
    KernelSQL.troncate_table(json);
    const head = json.columns.join("\t");
    const values = json.values.map((r: string[]) => r.join("\t")).join("\n");
    return `${head}\n${values}`;
  }

  /**
   * Format the result to fit Kernel API.
   */
  private formatResult(execResult: any) {
    if (execResult == null) return undefined;
    return {
      "text/plain": KernelSQL.text_table(execResult),
      "text/html": KernelSQL.html_table(execResult),
    };
  }

  /**
   * Execute SQL code and return an object containing columns names
   * (.columns) and values (.values).
   */
  private execCode(code: string, format: boolean = true) {
    let columns: string[] | undefined;
    let values: string[] | undefined;
    // see:
    // https://github.com/sql-js/sql.js/pull/388#issuecomment-712405389
    for (const statement of this.db.iterateStatements(code)) {
      columns = statement.getColumnNames();
      values = [];
      while (statement.step()) values.push(statement.get());
    }
    // only last statement is relevent
    if ((columns?.length ?? 0) === 0) return undefined;
    const res = { columns, values };
    if (format) return this.formatResult(res);
    return res;
  }

  /**
   * Run SQL code without returning anything.
   *
   * This is mainly done to bypass the db.run memory's limitation:
   * https://github.com/sql-js/sql.js/issues/482
   */
  private runCode(code: string) {
    // FIXME: this should be replaced by this.db.run(code) once
    // https://github.com/sql-js/sql.js/issues/482
    // is fixed since this should be faster than the
    // statement split used here...
    for (const statement of this.db.iterateStatements(code)) {
      while (statement.step());
    }
  }

  public async evalAsync(
    code: string,
    outCallback: (_: string) => void,
    errCallback: (_: string) => void,
    data: any = null
  ): Promise<[{ [key: string]: string } | undefined, number]> {
    type ReturnType = { [key: string]: string } | undefined;
    // force interactivity in all modes
    data.interactive = true;

    this._execution_count++;

    let result: ReturnType = undefined;
    const dotCommand = code.replace(/^[; \t\n]+|[; \t\n]+$/g, "");

    if (dotCommand in this._dots) {
      const callback = this._dots[dotCommand].callback as () => unknown;
      result = callback() as ReturnType;
    } else result = this.execCode(code) as ReturnType;
    return [result, this._execution_count];
  }

  public ps1() {
    return "sql> ";
  }

  public ps2() {
    return "...> ";
  }

  public restart() {
    // warning: we should probably reload module files...
    if (this.db) this.db.close();
    this._execution_count = 0;
    // reloading .db file
    this.db = new this.SQL.Database(this._dbInit);
    // foreign keys are activated by default
    // see: https://www.sqlite.org/foreignkeys.html#fk_enable
    this.runCode("PRAGMA foreign_keys = ON;");
    // reloading SQL modules files
    this._sqlsInit.forEach((c) => {
      this.runCode(c);
    });
  }

  public more(source: string) {
    return false;
  }

  public complete(code: string): [string[], number] | [] {
    return [];
  }

  public async putFile(filename: string, content: ArrayBuffer) {
    // just ignoring this since we don't have a filesystem.
  }

  public async putModule(filename: string, content: ArrayBuffer) {
    content = new Uint8Array(content);
    const ext = filename.split(".").pop();
    switch (ext) {
      case "sql":
        let decoder = new TextDecoder("utf-8");
        const _content = decoder.decode(content);
        this.runCode(_content);
        // SQL strings are concatenated
        this._sqlsInit.push(_content);
        break;
      case "sqlite":
      case "db":
        if (this.db) this.db.close();
        this.db = new this.SQL.Database(content);
        // initial db file is replaced
        this._dbInit = content;
        break;
      default:
        throw { message: "Only '.sql', '.db' and '.sqlite' files supported." };
    }
  }

  /**
   * Display help.
   */
  private _help() {
    const helpMessage = Object.keys(this._dots)
      .map((key: string) => `${key}\t\t${this._dots[key].help}`)
      .join("\n");
    return { "text/plain": helpMessage };
  }

  /**
   * Export (download) the database in binary format.
   */
  private _export() {
    this.download(this.db.export(), "database.db");
  }

  /**
   * Return the SQL commands to build the DB in a string.
   */
  private dump(): string {
    // order is creation order (order=false)
    const tables = this.tables(false)?.values ?? [];
    const schema = this.schema()?.values ?? [];
    const inserts = tables.map((table: string) => {
      const values = this.db.exec(`SELECT * FROM ${table}`)[0]?.values;
      const strValues = values.map(
        (value: any) => `(${value.map(JSON.stringify).join(", ")})`
      );
      return `INSERT INTO ${table} VALUES ${strValues.join(",\n")};`;
    });
    return `${schema.join(";\n")};\n${inserts.join("\n")}`;
  }

  /**
   * Dump (download) the database in SQL format (dot command version).
   */
  private _dump() {
    const encoder = new TextEncoder();
    const view = encoder.encode(this.dump());
    this.download(view, "database.sql");
  }

  /**
   * Return the DB schema.
   */
  private schema(order = false) {
    const orderCMD = order ? "ORDER BY tbl_name, type DESC, name" : "";
    return this.execCode(
      `SELECT sql FROM sqlite_schema WHERE type="table" ${orderCMD};`,
      false
    ) as { [key: string]: string[] } | undefined;
  }

  /**
   * Return the DB schema (dot command version).
   */
  private _schema() {
    return this.formatResult(this.schema());
  }

  /**
   * Return the table of tables.
   */
  private tables(order = true) {
    const orderCMD = order ? "ORDER BY 1" : "";
    return this.execCode(
      `SELECT name FROM sqlite_schema
WHERE type IN ('table','view') AND name NOT LIKE 'sqlite_%'
${orderCMD};`,
      false
    ) as { [key: string]: string[] } | undefined;
  }

  /**
   * Return the table of tables (dot command version).
   */
  private _tables() {
    return this.formatResult(this.tables());
  }
}
