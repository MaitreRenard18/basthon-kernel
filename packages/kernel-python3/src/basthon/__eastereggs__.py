import ast
import inspect
import webbrowser
from pyodide.console import CodeRunner

__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"

__all__ = []


class EllipsisRickroll(ast.NodeTransformer):
    """Easter egg that rick rolls the user whenever they use Ellipsis"""

    def visit_Ellipsis(self, node):
        webbrowser.open("https://youtu.be/dQw4w9WgXcQ?si=I_BiDnmkiL0KBA14")
        return node


i = -1
lyrics = [
    "Never gonna give you up",
    "Never gonna let you down",
    "Never gonna run around and desert you",
    "Never gonna make you cry",
    "Never gonna say goodbye",
    "Never gonna tell a lie and hurt you"
]


def get_lyrics(*_):
    global i, lyrics

    i += 1
    return lyrics[i % len(lyrics)]


class InverseCall(ast.NodeTransformer):
    """Easter egg that inverse function name and args,
    if a function is called normally it returns a rick roll
    (originally proposed by 2023-2024 NSI students at Henri Brisson, Vierzon):

    "hello"(print) -> print("hello")
    (1, 2)max -> max(1, 2)
    ([1, 2, 3, 4])min -> min([1, 2, 3, 4])
    len([1, 2, 3]) -> "Never gonna give you up"
    """

    def visit_Call(self, node):
        if len(node.args) <= 1:
            if isinstance(node.func, ast.Constant):
                node.func, node.args[0] = node.args[0], node.func

            elif isinstance(node.func, ast.List) or isinstance(node.func, ast.Tuple):
                node.func, node.args = node.args[0], node.func.elts

            else:
                node.func = ast.Name("get_lyrics", ast.Load())

        else:
            node.func = ast.Name("get_lyrics", ast.Load())

        return node


class OneAndOneIsThree(ast.NodeTransformer):
    """Easter egg proposed by 2023-2024 NSI students
    at Henri Brisson, Vierzon: 1 + 1 -> 1 + 2"""

    def visit_BinOp(self, node):
        if (
                isinstance(node.op, ast.Add)
                and isinstance(node.left, ast.Constant)
                and node.left.value == 1
                and isinstance(node.right, ast.Constant)
                and node.right.value == 1
        ):
            node.right.value = 2
        return node


# monkey-patch pyodide.console.CodeRunner to modify ast before compile
_unpatched_compile = CodeRunner.compile


def patched_compile(self, *args, **kwargs):
    for value in globals().values():
        if inspect.isclass(value) and issubclass(value, ast.NodeTransformer):
            value().visit(self.ast)
    return _unpatched_compile(self, *args, **kwargs)


CodeRunner.compile = patched_compile
