const gulp = require('gulp'),
  ts = require("gulp-typescript"),
  vinyl = require('vinyl'),
  merge = require('merge-stream');

/**
 * Get version number from lerna.json and put it in version.ts.
 */
gulp.task('version', function() {
  const pkg = require('../../lerna.json');
  const version = JSON.stringify(pkg.version);
  const src = require('stream').Readable({ objectMode: true });
  src._read = function() {
    this.push(new vinyl({
      cwd: "",
      path: "version.ts",
      contents: Buffer.from(`export const VERSION = ${version};`, 'utf-8')
    }));
    this.push(null);
  };
  return src.pipe(gulp.dest('src/'));
});

/**
 * Compiling kernel.
 */
gulp.task('kernel', function() {
  const tsProject = ts.createProject("tsconfig.json");
  const tsResult = tsProject.src().pipe(tsProject());
  return merge(tsResult, tsResult.js)
    .pipe(gulp.dest('lib/'));
});

gulp.task('all', gulp.series('version', 'kernel'));
