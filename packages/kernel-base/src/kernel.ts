import { PromiseDelegate } from "promise-delegate";
import { VERSION } from "./version";

const _localfsScheme = "filesystem:/";

/* we mock the fetch function to redirect queries to local FS */
(function () {
  const toDataURL = async (content: Uint8Array): Promise<string> => {
    return await new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      reader.onloadend = () => {
        resolve(reader.result as string);
      };
      reader.onerror = reject;
      reader.readAsDataURL(new Blob([content]));
    });
  };

  //@ts-ignore
  window._fetch = window._fetch ?? window.fetch;
  window.fetch = async function (request, opts) {
    //@ts-ignore
    const url = request.url ?? request.toString();
    const prefix = _localfsScheme;
    if (url.startsWith(prefix)) {
      const path = url.slice(prefix.length);
      let content = null;
      const options = {
        status: 200,
        statusText: "OK",
        headers: new Headers(),
      };
      try {
        //@ts-ignore
        content = window.Basthon.getFile(path);
        const dataURL = await toDataURL(content);
        const mime = dataURL.substring(
          dataURL.indexOf(":") + 1,
          dataURL.indexOf(";")
        );
        content = content.buffer;
        options.headers.append("Content-Type", mime);
        options.headers.append("Content-Length", content.byteLength);
      } catch (e) {
        options.status = 404;
        options.statusText = "Not Found";
      }
      return new Response(content, options);
    }
    //@ts-ignore
    return await window._fetch(request, opts);
  };

  const desc = Object.getOwnPropertyDescriptor(
    HTMLImageElement.prototype,
    "src"
  );
  Object.defineProperty(HTMLImageElement.prototype, "src", {
    ...desc,
    get: function () {
      return desc?.get?.call(this);
    },
    set: function (e) {
      const prefix = _localfsScheme;
      if (e.startsWith(prefix)) {
        const path = e.slice(prefix.length);
        //@ts-ignore
        const content = window.Basthon.getFile(path).buffer;
        (async () => desc?.set?.call(this, await toDataURL(content)))();
      } else {
        desc?.set?.call(this, e);
      }
    },
  });
})();

/**
 * An error thrown by not implemented API functions.
 */
class NotImplementedError extends Error {
  constructor(funcName: string) {
    super(`Function ${funcName} not implemented!`);
    this.name = "NotImplementedError";
  }
}

/**
 * Event for Basthon's dispatch/listen.
 */
class BasthonEvent extends Event {
  public detail: any;
  constructor(id: string, data: any) {
    super(id);
    this.detail = data;
  }
}

/**
 * API that any Basthon kernel should fill to be supported
 * in console/notebook.
 */
export class KernelBase {
  private _rootPath: string;
  private _ready: boolean = false;
  private readonly _loaded = new PromiseDelegate<void>();
  protected _execution_count: number = 0;
  protected _pendingInput: boolean = false;
  // a map to register wrapped listeners to allow later remove
  private _listeners = new Map<
    string,
    Map<EventListenerOrEventListenerObject, EventListenerOrEventListenerObject>
  >();

  constructor(options: any) {
    // root path where kernel is installed
    this._rootPath = options.rootPath;
  }

  /**
   * Kernel version number (string).
   */
  public version(): string {
    return VERSION;
  }

  /**
   * Language implemented in the kernel (string).
   * Generally lower case.
   */
  public language(): string {
    throw new NotImplementedError("language");
  }

  /**
   * Language name implemented in the kernel (string).
   * As it should be displayed in text.
   */
  public languageName(): string {
    throw new NotImplementedError("languageName");
  }

  /**
   * Script (module) file extensions
   */
  public moduleExts(): string[] {
    throw new NotImplementedError("moduleExts");
  }

  /**
   * Launch the kernel.
   */
  public async launch() {
    throw new NotImplementedError("launch");
  }

  /**
   * Execution count getter.
   */
  public get execution_count() {
    return this._execution_count;
  }

  /**
   * Async code evaluation that resolves with the result.
   */
  public evalAsync(
    code: string,
    outCallback: (_: string) => void,
    errCallback: (_: string) => void,
    data: any = null
  ): Promise<any> {
    throw new NotImplementedError("evalAsync");
  }

  public restart(): void {
    throw new NotImplementedError("restart");
  }

  public async putFile(filename: string, content: ArrayBuffer): Promise<void> {
    throw new NotImplementedError("putFile");
  }

  public async putModule(
    filename: string,
    content: ArrayBuffer
  ): Promise<void> {
    throw new NotImplementedError("putModule");
  }

  public userModules(): string[] {
    return [];
  }

  /**
   * Get a file content from the VFS.
   */
  public getFile(path: string): Uint8Array {
    throw new NotImplementedError("getFile");
  }

  /**
   * Get a user module file content.
   */
  public getUserModuleFile(filename: string): Uint8Array {
    throw new NotImplementedError("getUserModuleFile");
  }

  public more(source: string): boolean {
    throw new NotImplementedError("more");
  }

  public complete(code: string): [string[], number] | [] {
    return [];
  }

  public banner(): string {
    return `Welcome to the ${this.languageName()} REPL!`;
  }

  public ps1(): string {
    return ">>> ";
  }

  public ps2(): string {
    return "... ";
  }

  /**
   * Initialize the kernel.
   */
  public async init() {
    try {
      await this.launch();
    } catch (error) {
      this._loaded.reject(error);
      return;
    }
    // connecting eval to basthon.eval.request event.
    this.addEventListener("eval.request", this.evalFromEvent.bind(this));
    this._ready = true;
    this._loaded.resolve();
  }

  /**
   * Is the kernel ready?
   */
  public get ready() {
    return this._ready;
  }

  /**
   * Promise that resolve when the kernel is loaded.
   */
  public async loaded() {
    await this._loaded.promise;
  }

  /**
   * Root for kernel files. This is always the language directory
   * inside the version number directory inside the kernel directory.
   */
  public basthonRoot(absolute: boolean = false): string {
    let url = this._rootPath + "/" + this.version() + "/" + this.language();
    if (absolute && !url.startsWith("http")) {
      const base = window.location.origin + window.location.pathname;
      url = base.substring(0, base.lastIndexOf("/")) + "/" + url;
    }
    return url;
  }

  /**
   * Downloading data (bytes array or data URL) as filename
   * (opening browser dialog).
   */
  public download(data: Uint8Array | string, filename: string): void {
    if (!(typeof data === "string" || data instanceof String)) {
      const blob = new Blob([data], { type: "application/octet-stream" });
      data = window.URL.createObjectURL(blob);
    }
    const anchor = document.createElement("a");
    anchor.download = filename;
    anchor.href = data as string;
    anchor.target = "_blank";
    anchor.style.display = "none"; // just to be safe!
    document.body.appendChild(anchor);
    anchor.click();
    document.body.removeChild(anchor);
  }

  /**
   * Dynamically load a script asynchronously.
   */
  public static loadScript(url: string) {
    return new Promise<any>(function (resolve, reject) {
      let script = document.createElement("script");
      script.onload = resolve;
      script.onerror = reject;
      script.src = url;
      document.head.appendChild(script);
    });
  }

  /**
   * Wrapper around document.dispatchEvent.
   * It adds the 'basthon.' prefix to each event name and
   * manage the event lookup to retreive relevent data.
   */
  public dispatchEvent(eventName: string, data: any): void {
    document.dispatchEvent(new BasthonEvent(`basthon.${eventName}`, data));
  }

  /**
   * Wrapper around document.addEventListener.
   * It manages the 'basthon.' prefix to each event name and
   * manage the event lookup to retreive relevent data.
   */
  public addEventListener(eventName: string, callback: (_: any) => void): void {
    // wrapped callback
    const _callback = (event: Event) =>
      callback((event as BasthonEvent).detail);
    document.addEventListener(
      `basthon.${eventName}` as keyof DocumentEventMap,
      _callback
    );
    // register the wrapped callback in order to remove it
    let listeners = this._listeners.get(eventName);
    if (listeners == null) {
      listeners = new Map<
        EventListenerOrEventListenerObject,
        EventListenerOrEventListenerObject
      >();
      this._listeners.set(eventName, listeners);
    }
    listeners.set(callback, _callback);
  }

  /**
   * Wrapper around document.removeEventListener.
   * It manages the 'basthon.' prefix to each event name.
   */
  public removeEventListener(
    eventName: string,
    callback: (_: any) => void
  ): void {
    // get the wrapped callback
    const listeners = this._listeners.get(eventName);
    document.removeEventListener(
      `basthon.${eventName}` as keyof DocumentEventMap,
      listeners?.get(callback) as EventListenerOrEventListenerObject
    );
  }

  /**
   * Send eval.input event then wait for the user response and return it.
   */
  public async inputAsync(
    prompt: string | null | undefined,
    password: boolean = false,
    data: any = undefined
  ) {
    data = this.clone(data);
    data.content = { prompt, password };
    const promise = new Promise(function (resolve, reject) {
      data.resolve = resolve;
      data.reject = reject;
    });
    this._pendingInput = true;
    this.dispatchEvent("eval.input", data);
    const res = await promise;
    this._pendingInput = false;
    return res;
  }

  /**
   * Simple clone via JSON copy.
   */
  public clone(obj: any): any {
    // simple trick that is enough for our purpose.
    return JSON.parse(JSON.stringify(obj));
  }

  /**
   * Put a ressource (file or module).
   * Detection is based on extension.
   */
  public async putRessource(filename: string, content: ArrayBuffer) {
    const ext = filename.split(".").pop() ?? "";
    if (this.moduleExts().includes(ext)) {
      return await this.putModule(filename, content);
    } else {
      return await this.putFile(filename, content);
    }
  }

  /**
   * Is an input pending?
   */
  public pendingInput() {
    return this._pendingInput;
  }

  /**
   * Internal. Code evaluation after an eval.request event.
   */
  public async evalFromEvent(data: any) {
    const stdCallback = (std: string) => (text: string) => {
      let dataEvent = this.clone(data);
      dataEvent.stream = std;
      dataEvent.content = text;
      this.dispatchEvent("eval.output", dataEvent);
    };
    const outCallback = stdCallback("stdout");
    const errCallback = stdCallback("stderr");

    let args;
    try {
      args = await this.evalAsync(data.code, outCallback, errCallback, data);
    } catch (error: any) {
      errCallback(error.toString());
      const dataEvent = this.clone(data);
      dataEvent.error = error;
      dataEvent.execution_count = this.execution_count;
      this.dispatchEvent("eval.error", dataEvent);
      return;
    }
    if (args == null) return; // this should not happend
    const result = args[0];
    const executionCount = args[1];
    let dataEvent = this.clone(data);
    dataEvent.execution_count = executionCount;
    if (result != null) dataEvent.result = result;
    this.dispatchEvent("eval.finished", dataEvent);
  }
}
